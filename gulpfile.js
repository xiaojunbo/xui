/*
 gulp模块说明：

 gulp                         --- gulp主模块 http://www.ydcss.com/archives/18
 gulp-if                      --- gulp排除
 gulp-sass                    --- scss编译成css工具 https://www.npmjs.com/package/gulp-sass
 gulp-uglify                  --- 压缩js工具 http://www.ydcss.com/archives/54
 gulp-clean-css               --- 压缩css工具 http://www.ydcss.com/archives/41
 gulp-imagemin                --- 压缩图标工具 http://www.ydcss.com/archives/26
 gulp-rename                  --- 文件／文件夹重命名工具 https://www.npmjs.com/package/gulp-rename
 gulp-clean                   --- 清除文件工具 https://www.npmjs.com/package/gulp-clean
 gulp-connect                 --- 使用connect启动一个Web服务器
 gulp-rev-append              --- 文件版本替换

 */
var gulp     = require('gulp'),
    gulpif   = require('gulp-if'),
    sass     = require('gulp-sass'),
    uglify   = require('gulp-uglify'),
    mincss   = require('gulp-clean-css'),
    imagemin = require('gulp-imagemin'),
    rename   = require('gulp-rename'),
    clean    = require('gulp-clean'),
    connect  = require('gulp-connect'),
    shell    = require('gulp-shell'),
    rev      = require('gulp-rev-append');

/*
 $ gulp help  为参与本项目的开发者提供的的一些命令参考！
 */
gulp.task('help', function () {
    console.log('');
    console.log('        说明：  * 常用/重要    & 本命令');
    console.log('');
    console.log('        gulp help       & 帮助');
    console.log('        gulp item         了解整个项目结构');
    console.log('        gulp dev        * 开发环境全部打包');
    console.log('        gulp watch      * 文件监控打包');
    console.log('');
    console.log('        单任务命令：');
    console.log('        gulp sass         编译合并压缩scss');
    console.log('        gulp script       压缩js');
    console.log('        gulp image        压缩图片');
    console.log('        gulp fonts        打包字体（一般为iconfont）');
    console.log('        gulp clean        清理生产环境');
    console.log('');
    console.log('        gulp test         发布测试环境');
    console.log('');
});

/*
 $ gulp item  了解整个项目的目录结构与文档说明！
 */
gulp.task('item', function () {
    console.log('');
    console.log('        说明：/ 目录文件夹    * 重要／核心    & 本文件位置');
    console.log('');
    console.log('        xui.desktop');
    console.log('        ├── date/                                 用于调试的数据目录');
    console.log('        ├── dist/                                 编译打包后的文件目录( src -> dist )');
    console.log('        ├── docs/                                 开发说明文档');
    console.log('        ├── src/                                  开发源文件');
    console.log('        │   ├── fonts/                            字体目录，一般为iconfont');
    console.log('        │   ├── imgs/                             图标目录');
    console.log('        │   ├── js/                               js开发源文件');
    console.log('        │   │   ├── xui.js                        js开发源文件');
    console.log('        │   │   └── ux.js                         js开发源文件');
    console.log('        │   └── sass/                             scss开发源文件');
    console.log('        │       ├── custom/                       放一次性定制样式');
    console.log('        │       │   ├── custom.scss               定制的样式文件');
    console.log('        │       │   ├── iconfont.scss             定制的iconfont文件');
    console.log('        │       │   └── ...                       ');
    console.log('        │       ├── plugins/                      放第三方插件样式');
    console.log('        │       ├── xui/                        * MCUI样式目录');
    console.log('        │       │   ├── base/                     MCUI基础模块，全局性的模块，如重置，字体，色调，布局，网格等');
    console.log('        │       │   │   ├── mc-reset              ');
    console.log('        │       │   │   ├── mc-base               ');
    console.log('        │       │   │   ├── mc-size               ');
    console.log('        │       │   │   ├── mc-color              ');
    console.log('        │       │   │   └── ...                   ');
    console.log('        │       │   ├── frames/                    MCUI页面框架');
    console.log('        │       │   │   ├── frames-views/               ');
    console.log('        │       │   │   │   ├── index.html        ');
    console.log('        │       │   │   │   └── ...               ');
    console.log('        │       │   │   └── ...                   ');
    console.log('        │       │   ├── module/                   MCUI模块组件，UI组件，如按钮，输入框，导航，菜单等');
    console.log('        │       │   │   ├── xui-line.scss          ');
    console.log('        │       │   │   ├── xui-btn.scss           ');
    console.log('        │       │   │   └── ...                   ');
    console.log('        │       │   ├── theme/                    扩展的主题风格目录，一般情况下并不使用分离式主题，而是使用变量声明式切换主题，该目录仅为扩展保留');
    console.log('        │       │   │   ├── theme.scss            默认主题风格');
    console.log('        │       │   │   └── ...                   ');
    console.log('        │       │   ├── xui.scss                  MCUI主体文件，包含了MCUI所有模块');
    console.log('        │       │   └── xui-low.scss              MCUI主体文件-兼容IE低版本，包含了MCUI所有模块');
    console.log('        │       ├── main.scss                     扩展的主题风格目录，一般情况下并不使用分离式主题，而是使用变量声明式切换主题，该目录仅为扩展保留');
    console.log('        │       └── ...                           默认主题风格');
    console.log('        ├── views/                                静态页面／模板');
    console.log('        ├── index.html                            入口文件');
    console.log('        └── gulpfile.js                         & gulp任务文件');
    console.log('');

});

var xui   = './src/sass/xui/';
// 调用框架
var frame = './src/xui/frames/';
// 调用风格
var theme = './src/sass/xui/theme/';

// 编译压缩
var test = 'dist';


// npm
gulp.task('xui', shell.task([
    'npm install --save-dev xui'
]));

// 框架页面
gulp.task('frame', function () {
    gulp.src(frame + 'frames-views/index.html')
        .pipe(rev())
        .pipe(gulp.dest(''));
});

// 编译压缩scss任务
gulp.task('xui-sass', function () {
    // 全部样式合并打包
    gulp.src(xui + '*.scss')                                           //需要编译的xui文件夹路经
        .pipe(sass())                                                  //编译scss
        .pipe(mincss())                                                //压缩编译好的css
        .pipe(rename({suffix: '.min'}))                                //压缩的css重命名为*.min.css
        .pipe(gulp.dest(test + '/css'));                               //编译压缩好的css文件输出到测试区（test）

});

// 编译压缩scss任务
gulp.task('sass', function () {
    // 全部样式合并打包
    gulp.src('src/sass/*.scss')                                        //需要编译的scss文件夹路经
        .pipe(sass())                                                  //编译scss
        .pipe(mincss())                                                //压缩编译好的css
        .pipe(rename({suffix: '.min'}))                                //压缩的css重命名为*.min.css
        .pipe(gulp.dest(test + '/css'))                                //编译压缩好的css文件输出到测试区（test）
        .pipe(gulp.dest(frame + 'frames-views/dist/css'));              //为了配合rev版本对比。
});

// 压缩js文件任务
gulp.task('script', function () {
    gulp.src(['src/js/**/*.js', '!src/js/**/*.min.js'])                //需要压缩的js文件
        .pipe(uglify())                                                //压缩js文件
        .pipe(rename({suffix: '.min'}))                                //改文件名为*.min.js
        .pipe(gulp.dest(test + '/js'))                                 //压缩好的js输出路经
        .pipe(gulp.dest('src/js'))                                     //为了开发中文件路经不报错
        .pipe(gulp.dest(frame + 'frames-views/dist/js'));               //

    gulp.src(['src/js/**/*.min.js', 'src/js/**/*.map'])
        .pipe(gulp.dest(test + '/js'))
        .pipe(gulp.dest(frame + 'frames-views/dist/js'));
});

// 压缩图片任务
gulp.task('image', function () {
    gulp.src('src/imgs/*.{png,jpg,gif,ico}')
        .pipe(imagemin({
            optimizationLevel: 8,                                      //类型：Number  默认：3  取值范围：0-7（优化等级）
            testgressive     : true,                                   //类型：Boolean 默认：false 无损压缩jpg图片
            interlaced       : true,                                   //类型：Boolean 默认：false 隔行扫描gif进行渲染
            multipass        : true                                    //类型：Boolean 默认：false 多次优化svg直到完全优化
        }))
        .pipe(gulp.dest(test + '/imgs'));                              //压缩好的图片输出路经
});

// 图标字体作务
gulp.task('fonts', function () {
    return gulp.src('src/fonts/*.{eot,svg,ttf,woff}')                  //挑选需要的iconfont文件（demo等不需要的不选择）
        .pipe(gulp.dest(test + '/fonts'));                             //从开发区（dev）移到打包区（test）文件夹
});

// 自动监视任务
gulp.task('watch', function () {
    gulp.watch(frame + '**/*.html', ['frame']);                        //自动监视frame
    gulp.watch('src/js/*.js', ['script']);                             //自动监视js变化并执行js(参考sass作务)压缩任务
    gulp.watch('src/sass/**/*.scss', ['sass']);                        //自动监视scss变化并执行sass(参考sass作务)任务。
});

// 清场测试区任务
gulp.task('clean', function () {
    return gulp.src([test, '!src/'], {read: false})                    //清除test文件夹，以免留下不需要的残留文件。
        .pipe(clean());
});

gulp.task('connect', function () {
    connect.server({
        host      : '',                                                //地址，可不写，不写的话，默认localhost
        port      : 3000,                                              //端口号，可不写，默认8000
        root      : './test',                                          //当前项目主目录
        livereload: true                                               //自动刷新
    });
});

/*
 gulp 默认任务依次是清场打包(dist)文件夹，清场完成后开始编译压缩scss文件，压缩js文件，压缩图片，图标字体。
 */
gulp.task('default', ['xui', 'clean'], function () {
    gulp.start(['frame', 'sass', 'script', 'image', 'fonts', 'npm']);
});


// npm包
gulp.task('npm-clean', function () {
    return gulp.src(['!./npm/xxd-xui/.git','!./npm/xxd-xui/.gitignore','!./npm/xxd-xui/.npmignore','!./npm/xxd-xui/package.json', './npm/xxd-xui/*', '!src/'], {read: false}).pipe(clean());
});

gulp.task('build-npm', function () {
    gulp.src('./src/xui/**').pipe(gulp.dest('npm/xxd-xui'));
});

gulp.task('npm', ['npm-clean'], function () {
    gulp.start(['build-npm']);
});