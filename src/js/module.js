﻿/*
 *  模块组件！
 *  mc 模块组件/复合组件 --module components
 **/
var mc = {
    tabs: function () {
        /*
         * 选项卡组件！
         * 标准结构2级
         * 属性：
         *  data-area    容器（在根容器设置）
         * 示例：
         *  <ul class="xui-tabs" data-area="#id1">
         *      <li class="on">a</li>
         *      <li>b</li>
         *      <li>c</li>
         *  </ul>
         *
         *  <div id="id1" class="xui-tabs-area">
         *      <div class="on">a content</div>
         *      <div>b content</div>
         *      <div>c content</div>
         *  </div>
         */

        $(document).on('click', '.xui-tabs>*', function () {
            var area = $($(this).parent().data('area'));
            var index = $(this).index();
            $(this).addClass('on').siblings().removeClass('on');
            area.children().eq(index).addClass('on').siblings().removeClass('on');
            console.log('xui-tabs')
        })
    },
    navs: function () {
        /*
         * 导航条组件！
         * data-area    容器（在根容器设置）
         * data-tpl     载入模版（在事件对象设置）
         */

        $(document).on('click', '.xui-navs>*', function () {
            var area = $($(this).parent().data('area'));
            var index = $(this).index();
            $(this).addClass('on').siblings().removeClass('on');
            area.children().eq(index).addClass('on').siblings().removeClass('on');
            console.log('xui-tabs')
        })

    }

};