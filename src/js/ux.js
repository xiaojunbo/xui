﻿define(['jquery', 'tpl', 'data'], function ($, tpl, data) {
    var ux = {
        onHash   : function (route) {
            console.log('获取传参');
            console.log('↓');
            console.log(route);
            var thisRoute = route.split('/');
            console.log('处理传参是否是多级页面');
            console.log('↓');
            console.log(thisRoute);

            var itError = 1;
            for (var i = 0; i < thisRoute.length; i++) {
                if (tpl[thisRoute[i]]) {
                    itError *= 1
                } else {
                    itError *= 0
                }
            }
            if (itError === 1) {
                window.location.hash = 'route=' + route;
            } else {
                window.location.hash = 'route=error';
            }

        },
        onRoute  : function () {
            var thisHash = window.location.hash || '#route=home';
            // console.log('获取网址参数 ' + thisHash);
            if(thisHash.search(/route=/ig) > 0){
                var route    = thisHash.match(/(?:#route=)([\w\/]+)(?:[&?|]?)/)[1].split('/');
                // console.log('获取网址参数的路由部份 ' + route);
                // var subs     = route.length;
                // console.log('获取网址参数的路由部份层级 ' + subs);

                for (var i = 0; i < route.length; i++) {
                    if (tpl[route[i]]) {

                    } else {
                        route[i] = 'error';
                    }
                }
                ux.onLoad(0, route);

            }else {
                ux.onHash('home');
            }

        },
        onLoad   : function (i, route) {
            // ux.oLoading();

            if (i === route.length) {
                // console.log('end->  关闭动画');
                // setTimeout(function () {
                //     ux.xLoading();
                // }, 500);

                return i;
            } else {

                if(route[i] == 'error'){
                    alert('错误页')
                }else{
                    var oFrame = $(tpl[route[i]].frame), oUrl = tpl[route[i]].url;

                    // oFrame.html('<div class="ui-loading">加哉中...</div>');

                    // console.log(oFrame);
                    // console.log(oFrame+'  ->  '+oUrl);

                    oFrame.load(oUrl, function (response, status, xhr) {
                        if (status == "success") {

                            $('.o_route[route=' + route[i] + ']').addClass('on').siblings().removeClass('on');

                            return ux.onLoad(i + 1, route);

                        } else {
                            ux.oError('err');
                        }
                    });
                }

            }

        }
        ,
        oLoading : function () {
            $('#start').stop().fadeIn(0);
        }
        ,
        xLoading : function () {
            $('#start').stop().fadeOut(0);
        }
        ,
        openRoute: function (name) {
            // console.log(name);
        }
        ,
        oError   : function (err) {
            alert('数据加载错误' + err);
            // console.log('数据加载错误' + err);
        }

    };
    return ux;
});

