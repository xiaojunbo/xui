﻿define(function () {
    var tpl = {
        error      : {frame: "#view_main", name: "error", title: "错误页", url: "views/error.html"},
        home       : {frame: "#view_main", name: "home", title: "首页", url: "views/home.html"},
        start      : {frame: "#view_main", name: "start", title: "开始", url: "views/start.html"},
        standards  : {frame: "#view_main", name: "standards", title: "规范", url: "views/standards.html"},
        components : {frame: "#view_main", name: "components", title: "组件", url: "views/components.html"},
        plugins    : {frame: "#view_main", name: "plugins", title: "插件", url: "views/plugins.html"},
        template   : {frame: "#view_main", name: "template", title: "模版", url: "views/template.html"},
        design     : {frame: "#view_main", name: "design", title: "图形", url: "views/design.html"},
        contributor: {frame: "#view_main", name: "contributor", title: "贡献", url: "views/contributor.html"},
        questions  : {frame: "#view_main", name: "questions", title: "问答", url: "views/questions.html"},

        reset: {frame: "#mid-l", name: "reset", title: "全局重置", url: "views/components/reset.html"},
        base : {frame: "#mid-l", name: "base", title: "基础预设", url: "views/components/base.html"},
        size : {frame: "#mid-l", name: "size", title: "全局字号", url: "views/components/size.html"},
        color: {frame: "#mid-l", name: "color", title: "全局色调", url: "views/components/color.html"},
        theme: {frame: "#mid-l", name: "theme", title: "全局主题", url: "views/components/theme.html"},

        grid   : {frame: "#mid-l", name: "grid", title: "栅格系统", url: "views/components/grid.html"},
        select : {frame: "#mid-l", name: "select", title: "下拉框", url: "views/components/select.html"},
        width  : {frame: "#mid-l", name: "width", title: "预设宽度", url: "views/components/width.html"},
        height : {frame: "#mid-l", name: "height", title: "预设高度", url: "views/components/height.html"},
        padding: {frame: "#mid-l", name: "padding", title: "预设内边距", url: "views/components/padding.html"},
        margin : {frame: "#mid-l", name: "margin", title: "预设外边距", url: "views/components/margin.html"},
        file   : {frame: "#mid-l", name: "file", title: "上传", url: "views/components/file.html"},
        paging : {frame: "#mid-l", name: "paging", title: "分页", url: "views/components/paging.html"},

        button  : {frame: "#mid-l", name: "button", title: "按钮", url: "views/components/button.html"},
        input   : {frame: "#mid-l", name: "input", title: "输入框", url: "views/components/input.html"},
        hint    : {frame: "#mid-l", name: "hint", title: "提示", url: "views/components/hint.html"},
        radio   : {frame: "#mid-l", name: "radio", title: "单选框", url: "views/components/radio.html"},
        checkbox: {frame: "#mid-l", name: "checkbox", title: "复选框", url: "views/components/checkbox.html"},
        switch  : {frame: "#mid-l", name: "switch", title: "开关", url: "views/components/switch.html"},
        tag     : {frame: "#mid-l", name: "tag", title: "标签", url: "views/components/tag.html"},
        popup   : {frame: "#mid-l", name: "popup", title: "弹出框", url: "views/components/popup.html"},

        table: {frame: "#mid-l", name: "table", title: "表格", url: "views/components/table.html"},

        xiaojunbo: {frame: "#view_main", name: "xiaojunbo", title: "肖军波", url: "views/xiaojunbo.html"}
    };
    return tpl;
});

