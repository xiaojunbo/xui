﻿/*
 *  模块组件说明！
 *      ux 全站交互！
 **/
require.config({
    paths: {
        jquery: "jquery.min",
        tpl    : "tpl.min",
        ux    : "ux.min",
        data  : "data.min"
    },
    waitSeconds:100000000000
});
require(['jquery', 'tpl', 'ux', 'data'], function ($, tpl, ux, data) {
    $(function () {
        ux.onRoute();
        $(document).on('click', '.o_route', function () {
            var route = $(this).attr('route-more') || $(this).attr('route');
            $(this).addClass('on').siblings().removeClass('on');
            ux.onHash(route);
        });

        window.onhashchange = function () {
            ux.onRoute();
        };

    });

    $(window).resize(function () {

    });
});
