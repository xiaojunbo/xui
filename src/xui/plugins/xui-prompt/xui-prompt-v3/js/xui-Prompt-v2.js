var xui = {
    Prompt: {
        task: [],
        idle: true,
        init: function (conf) {
            var tasks = xui.Prompt.task;
            var idle  = xui.Prompt.idle;
            console.log('实例开始');
            console.log('↓');
            console.log('准备参数');
            console.log('↓');
            console.log(conf);
            console.log('↓');
            console.log('实例加入队列中');
            tasks.push(conf);
            console.log('↓');
            console.log('当前队列共有' + tasks.length + '个任务');
            console.log('↓');
            console.log(tasks);
            console.log('↓');
            console.log('当前页面是否空闲');
            console.log('↓');
            console.log(idle);
            console.log('↓');
            console.log('如果页面空闲则显示一个队列中的实例，否则不显示实例但显示队列数量');
            console.log('↓');
            if (idle) {
                console.log('显示队列中的第一个实例');
                console.log('↓');
                console.log('同时从队列中删除该实例');
                console.log('↓');
                console.log('同时设置空间属性为false');

            }else {
                console.log('不显示实例但是显示队列数量');
                console.log('↓');
            }

        },
        show: function (conf) {
            var id   = conf.id,
                dim  = conf.dim || false,
                pos  = conf.pos || 'cc',
                time = conf.time || 2000,
                icon = conf.icon,
                msg  = conf.msg || '',
                mod  = '.xui-prompt';

            var lineUp = [];

            if ($(mod + '[dim="true"]').length > 0) {
                dim = false;
            }
            if (icon) {
                icon = '<p class="prompt-icon" icon="' + icon + '"><b></b></p>';
            } else {
                icon = '';
            }
            if (!id) {
                id = '#prompt' + ( $(mod).length * 1 + 1);
            }
            if (msg) {
                $('.xui-prompt').remove();
                var code = '<div style="display:none" id="' + id + '" class="xui-prompt" dim="' + dim + '" pos="' + pos + '"><div class="xui-prompt-dim"><div class="xui-prompt-content">' + icon + '<p>' + msg + '</p></div></div></div>';
                var $id  = $(code);

                lineUp.unshift({id: id, code: code});

                console.log(lineUp);

                if ($(mod).length === 0) {
                    $('body').addClass('n_move').append($id);
                    lineUp.splice(1, 1);
                    console.log(lineUp);
                }

                setTimeout(function () {
                    $id.stop().fadeIn(200);
                }, 100);

                setTimeout(function () {
                    $id.stop().fadeOut(function () {
                        $('.xui-prompt').remove();
                        $('body').removeClass('n_move');
                    });

                    if (lineUp.length > 0) {
                        $('body').addClass('n_move').append(lineUp[0].code);
                        lineUp.splice(1, 1);
                        console.log(lineUp);
                    }

                }, parseInt(time));

                return $id;
            }
        },
        hide: function () {
            $('.xui-prompt').remove();
            $('body').removeClass('n_move');
        }

    }
};