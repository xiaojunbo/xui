var xui = {
    Prompt: function (conf) {
        var id   = conf.id,
            dim  = conf.dim || false,
            theme  = conf.theme || 'default',
            time = conf.time || 2000,
            icon = conf.icon,
            msg  = conf.msg || '',
            mod  = '.xui-prompt';

        if ($(mod + '[dim="true"]').length > 0) {
            dim = false;
        }
        if (icon) {
            icon = '<p class="prompt-icon" icon="' + icon + '"><b></b></p>';
        } else {
            icon = '';
        }
        if (!id) {
            id = '#prompt' + ( $(mod).length * 1 + 1);
        }
        if (msg) {
            $('.xui-prompt').remove();
            var code = '<div style="display:none" id="' + id + '" class="xui-prompt" dim="' + dim + '" theme="' + theme + '"><div class="xui-prompt-dim"><div class="xui-prompt-content">' + icon + '<p class="xui-prompt-msg">' + msg + '</p></div></div></div>';
            var $id  = $(code);

            if ($(mod).length === 0) {
                $('body').addClass('prompt-o-hide').append($id);
            }

            setTimeout(function () {
                $id.stop().fadeIn(200);
            }, 100);

            setTimeout(function () {
                $id.stop().fadeOut(function () {
                    $('.xui-prompt').remove();
                    $('body').removeClass('prompt-o-hide');
                });

            }, parseInt(time));

            return $id;
        }

    }
};