**目录结构：**
<br>
       
        说明：/ 目录文件夹    * 重要／核心
        
        xxd-xui
        ├── date/                          用于调试的数据目录
        ├── dist/                          编译打包后的文件目录( src -> dist )
        ├── docs/                          开发说明文档
        ├── src/                           开发源文件
        │   ├── fonts/                     字体目录，一般为iconfont
        │   ├── imgs/                      图标目录
        │   ├── js/                        js开发源文件
        │   │   ├── main.js                公共JS
        │   │   ├── ux.js                  交互JS
        │   │   └── ...                  
        │   └── sass/                      scss开发源文件
        │       ├── custom/                放一次性定制样式
        │       ├── plugins/               放第三方插件样式
        │       ├── xui/                 * xui样式目录
        │       │   ├── base/              xui基础模块，全局性的模块，如重置，字体，色调，布局，网格等
        │       │   │   ├── xui-reset
        │       │   │   ├── xui-base       
        │       │   │   ├── xui-size       
        │       │   │   ├── xui-color      
        │       │   │   └── ...            
        │       │   ├── module/            xui模块组件，UI组件，如按钮，输入框，导航，菜单等
        │       │   │   ├── xui-line       
        │       │   │   ├── xui-btn
        │       │   │   └── ...            
        │       │   ├── xui.scss           xui主体文件，包含了xui所有模块
        │       │   └── xui-low.scss       xui主体文件-兼容IE低版本，包含了xui所有模块
        │       └── theme/                 扩展的主题风格目录，一般情况下并不使用分离式主题，而是使用变量声明式切换主题，该目录仅为扩展保留
        │           └── default.scss       默认主题风格
        ├── views/                         静态页面／模板
        ├── index.html                     入口文件
        └── gulpfile.js                    gulp任务文件


**目录结构：**
<br>


        xui-module
        ├── base/                          
        │   ├── xui-mixin.scss             sass预设
        │   ├── xui-reset.scss             全局重置
        │   ├── xui-base.scss              基础预设
        │   ├── xui-color.scss             全局色调
        │   ├── xui-size.scss              全局字号
        │   ├── xui-cols.scss              栅格系统
        │   ├── xui-layout.scss            布局预设
        │   └── ...                      
        ├── module/                       
        │   ├── .xui-line                  分融线
        │   ├── .xui-btn	               按钮
        │   ├── .xui-input	               输入框
        │   ├── .xui-textarea	           文本域
        │   ├── .xui-rodia	               单选
        │   ├── .xui-checkbox	           复选
        │   ├── .xui-switch	               开关
        │   ├── .xui-navs	               导航
        │   ├── .xui-menu	               菜单
        │   ├── .xui-select	               下拉选择
        │   ├── .xui-tabs	               选项卡
        │   ├── .xui-table	               表格
        │   ├── .xui-hint	               提示
        │   ├── .xui-lable	               标签
        │   ├── .xui-list	               列表
        │   ├── .xui-card	               卡片
        │   ├── .xui-toast	               对话框
        │   ├── .xui-bar	               条栏
        │   ├── .xui-tobar	               顶栏
        │   ├── .xui-fobar	               底栏
        │   ├── .xui-badge	               角标
        │   ├── .xui-play	               轮播图
        │   ├── .xui-slide	               侧滑
        │   ├── .xui-number	               数字加减
        │   ├── .xui-grids	               格子(9宫格)
        │   ├── .xui-countdown	           倒计时
        │   ├── .xui-search	               搜索框
        │   ├── .xui-paging	               分页
        │   ├── .xui-file	               上传
        │   ├── .xui-layer	               弹出层
        │   ├── .xui-prompt	               提示框
        │   └── ...
        └── xui.scss